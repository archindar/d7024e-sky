package main

import (
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"regexp"
)

var (
	addr = flag.Bool("addr", false, "find open address and print to final-port.txt")
)

//Läser in alla template files vi har och gör om dem till templates. Onödigt att göra det i renderTemplates varje gång.
var templates = template.Must(template.ParseFiles("edit.html", "view.html")) //template.Must är en convenience wrapper, panikar om den får non-nil error. Returnerar annars templaten som den är.
//ParseFiles tar våra template files. Har bara edit och view just nu. Alla filer vi har läggs till som argument.
//ParseFiles gör om filerna till templates av samma namn.

//This is a validation expression.
var validPath = regexp.MustCompile("^/(edit|save|view)/([a-zA-Z0-9]+)$") //Panics if the expression compilation fails

//Data struct for interconnected pages
type Page struct {
	Title string
	Body  []byte
}

func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	//fmt.Println(p)*/
	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "edit", p) //Reads contents of edit and return a template                  //Executes template and writes the generated HTML to http.ResponseWriter
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")                  //Här hämtas värdena från formuläret
	p := &Page{Title: title, Body: []byte(body)} //En ny page skapas
	err := p.save()                              //Sidan sparas ned med title och body
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) { //Return a HandlerFunc
		m := validPath.FindStringSubmatch(r.URL.Path) //extract the page title from request
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fmt.Println(m)
		fn(w, r, m[2]) //Kallar argumentet till funktionen, en handler som är save, edit eller view.
	}
}

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {
	flag.Parse()
	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler)) //Alla URLs med inledning /edit/ hanteras av editHandler
	http.HandleFunc("/save/", makeHandler(saveHandler))

	if *addr {
		l, err := net.Listen("tcp", "127.0.0.1:0")
		if err != nil {
			log.Fatal(err)
		}
		if err != nil {
			log.Fatal(err)
		}
		s := &http.Server{}
		s.Serve(l)
		return
	}

	http.ListenAndServe("localhost:8080", nil)
}
