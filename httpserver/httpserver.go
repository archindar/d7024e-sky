package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hello,")
	r.ParseForm()
	fmt.Fprintf(w, r.FormValue("yourname"))
}
func main() {
	http.HandleFunc("/submit", handler)
	http.Handle("/", http.FileServer(http.Dir("./")))
	http.ListenAndServe(":8080", nil)
}
