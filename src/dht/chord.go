package dht

import (
	_ "crypto/sha1"
	"errors"
	"fmt"
	"github.com/boltdb/bolt"
	_ "github.com/nu7hatch/gouuid"
	"log"
	"math/big"
	"math/rand"
	"net"
	"net/rpc"
	"os"
	"strconv"
	"sync"
	"time"
)

const m = 160 //the lenght of the node id in bits. Also the number of slots in a finger table.
const r = 4   //O(log N)
var glob_port string
var glob_ip string

type Node struct { //The local node
	id       big.Int
	ip       string
	port     string
	succ     *Bro //pointer to the successor Node
	pred     *Bro //pointer to the predecessing Node. Should contain Chord ID and IP of the pred. Node
	finger   [m]*Finger
	succList [r]*Bro
	mutex    sync.Mutex
	db       *bolt.DB
	listener net.Listener
}

//represents a remote Node
type Bro struct { //The remote node
	Id   big.Int
	Ip   string
	Port string
}

type Finger struct {
	start big.Int
	succ  *Bro
}

//arguments relevant to Nodes and Bros
type Args struct {
	I   int
	S   Bro
	Key big.Int
}

//Creates a Bro out of a Node. This is to allow for a server to send itself to another server
func (this *Node) nodeToBro() Bro {
	// abro.Id = this.id
	// abro.Ip = this.ip
	// abro.Port = this.port
	return Bro{Id: this.id, Ip: this.ip, Port: this.port}
}

func (this *Node) print() string {
	return this.id.String() + "\n" + this.ip + "\n" + this.port + "\n" + this.succ.print() + "\n" + this.pred.print()
}

func (this *Bro) print() string {
	return this.Id.String() + "\n" + this.Ip + "\n" + this.Port
}

// Helper function. Checks if the Bro has not been assigned
func (this *Bro) Empty() bool {
	return this.Id.BitLen() == 0 && this.Port == "" && this.Ip == ""
}

// Helper function. Tries to connect to a node in orde to check if it is still alive
func (this *Bro) alive() bool {
	client, err := rpc.DialHTTP("tcp", this.Ip+":"+this.Port)
	if err != nil {
		log.Println("Dead node:", this, err)
		return false
	}
	err = client.Close()
	if err != nil {
		log.Fatal("Closing conn error:", err)
	}
	return true
}

//Creates a new node. Returns a new Node object containing that nodes ID, IP address and port
func makeDHTNode(id *big.Int, address string, port string) *Node {
	n := new(Node)

	if id == nil { // Ges inget id så skall vi generar ett nytt

		var id_int big.Int
		id_int.SetString(generateNodeId(), 16)
		//n.id = id_int.Bytes()
		n.id = id_int
		n.ip = address
		n.port = port
		n.succ = &Bro{} //initiation is required to aovid derefference errors that occurs when we want to
		n.pred = &Bro{} //swap the object which succ points on, to another new bro object
	} else {
		n.id = *id
		n.ip = address
		n.port = port
		n.succ = &Bro{} //initiation is required to aovid derefference errors that occurs when we want to
		n.pred = &Bro{} //swap the object which succ points on, to another new bro object
	}
	return n
}

/*##########################################
Adds a node to the node ring.
The function will ask another node (elder) for the correct successor to 'this' and
update the successor that it has a new predecessor
*/

func (this *Node) addToRing(elder *Bro) {
	var reply Bro //RPC return value
	address := elder.Ip + ":" + elder.Port
	client, err := rpc.DialHTTP("tcp", address)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	// err = client.Call("Node.GetPred", Args{}, &reply)
	// if err != nil {
	// 	log.Fatal("GetSucc error:", err)
	// }
	if client.Call("Node.GetPred", Args{}, &reply) != nil { //empty ring
		// 1 - set successor and predecessor
		*this.succ = *elder
		*this.pred = *elder

		// 2 - update predecessor at the other node
		var out string
		arg := Args{S: (this.nodeToBro())}
		fmt.Println("this as bro:", arg.S)
		err = client.Call("Node.Notify", arg, &out)
		if err != nil {
			log.Fatal("Notify error:", err)
		}
		fmt.Println("klar med notify")

		// 3 - create the first fingers on this node
		this.FirstInitFingers(Args{S: *elder}, &out) //the new node will set itself as successor on all fingers

		// 4 - on the other node, create the first fingers
		err = client.Call("Node.FirstInitFingers", Args{S: this.nodeToBro()}, &out)
		if err != nil {
			log.Fatal("FirstInitFingers error:", err)
		}

		fmt.Println("Nu finns två noder i ringen med följande egenskaper:")
		fmt.Println("Nod 1: ", this.print())
		//this.printFingerTable()
		//fmt.Println("Node 2: ")
		//ny.printFingerTable()
		err = client.Close()
		if err != nil {
			log.Fatal("Closing conn error:", err)
		}
	} else { //if the ring previously was non-empty
		// 1 - find successor
		var succ Bro
		// client, err := rpc.DialHTTP("tcp", elder.Ip+elder.Port)
		// if err != nil {
		// 	log.Fatal("dialing:", err)
		// }
		err = client.Call("Node.Lookup", Args{Key: this.id}, &succ)
		if err != nil {
			log.Fatal("Lookup error:", err)
		}
		*this.succ = succ
		err = client.Close()
		if err != nil {
			log.Fatal("Closing conn error:", err)
		}

		// 2 - find your predecessor
		var pred Bro
		var out string
		address2 := succ.Ip + ":" + succ.Port
		client2, err2 := rpc.DialHTTP("tcp", address2)
		if err2 != nil {
			log.Fatal("dialing:", err2)
		}
		err2 = client2.Call("Node.GetPred", Args{}, &pred) //update the predecessor in successor
		if err2 != nil {
			log.Fatal("Notify error:", err2)
		}
		*this.pred = pred
		// 3 - notify successor about new predecessor
		err2 = client2.Call("Node.Notify", Args{S: this.nodeToBro()}, &out) //Update predecessor in successorn
		if err2 != nil {
			log.Fatal("Notify error:", err2)
		}
		err2 = client2.Close()
		if err2 != nil {
			log.Fatal("Closing conn error:", err2)
		}
		fmt.Println("Nod: ", this.id.String())
		this.createSuccessorList()
		this.initFingers(&succ)
		this.updateOthers()
	}
}

//---------------this is the "find_successor" function from the paper. This func should replace func lookup--------------------------------------------------------------------------

func (this *Node) Lookup(arg Args, bro *Bro) error {
	//locks the node until it has been read
	this.mutex.Lock()
	defer this.mutex.Unlock()

	preceed := (this.find_predecessor(arg.Key))

	// Get the succesor that we are looking for

	if preceed.Id.Cmp(&this.id) == 0 { // predecessor is this local node
		fmt.Println("nej men det är ju oss själva")
		*bro = *this.succ

	} else {
		var reply Bro
		client, err := rpc.DialHTTP("tcp", preceed.Ip+":"+preceed.Port)
		if err != nil {
			log.Fatal("Lookup dialing:", err)
		}
		err = client.Call("Node.GetSucc", Args{}, &reply)
		if err != nil {
			log.Fatal("Lookup GetSucc error error:", err)
		}
		err = client.Close()
		if err != nil {
			log.Fatal("Lookup Closing conn error:", err)
		}
		*bro = reply
	}
	return nil
}

//finds the node laying just before key. Returns the new node
func (this *Node) find_predecessor(key big.Int) Bro {

	//locks the node until it has been modified
	// this.mutex.Lock()
	// defer this.mutex.Unlock()

	// fmt.Println("Inne i find_predecessor")
	n_temp := this.nodeToBro()
	// fmt.Println("Nu ska vi loopa igenom betweenTo")
	var reply, succ Bro
	succ = *this.succ

	for !(betweenTo(n_temp.Id, succ.Id, key)) { //as long as key is not located between n_temp and its successor, n_temp is not the node laying immedietely before
		//fmt.Println("Key: ", key.String(), " låg inte mellan ", n_temp.Id.String(), "och ", n_temp.succ.Id.String())

		// 1 - Retrieve the closests preceding finger
		client, err := rpc.DialHTTP("tcp", n_temp.Ip+":"+n_temp.Port)
		if err != nil {
			log.Fatal("dialing:", err)
		}
		err = client.Call("Node.Closest_preceeding_finger", Args{Key: key}, &reply)
		if err != nil {
			log.Fatal("Closest_preceeding_finger error:", err)
		}

		if n_temp.Id.Cmp(&reply.Id) == 0 { // Find which of n_temp's fingers laying closest to key, going from n_temp
			err = client.Close()
			if err != nil {
				log.Fatal("Closing conn error:", err)
			}
			fmt.Println("bryt")
			break //Stop if we only finds the same node

		} else {
			n_temp = reply //set n_temp to the current node
		}

		// 2 - return the successor for the new n_temp
		client2, err2 := rpc.DialHTTP("tcp", n_temp.Ip+":"+n_temp.Port)
		if err2 != nil { // Cannot connect to the the node

			fmt.Println("dialing nr1:", err2)
			time.Sleep(2 * time.Second)
			client2, err2 = rpc.DialHTTP("tcp", n_temp.Ip+":"+n_temp.Port)
			if err2 != nil { // Second failure = Node considered dead
				fmt.Println("dialing nr1:", err2)
				// find the closest preceeding node to the failed node
				var just_before_hash big.Int
				var avan int = 0                  // loop variable
				for (err2 != nil) && (avan < r) { // find the node that lies just before the failed one. Loop if that one also is failed
					err = client.Call("Node.Closest_preceeding_finger", Args{Key: *just_before_hash.Sub(&n_temp.Id, big.NewInt(1))}, &reply)
					if err != nil {
						log.Fatal("Closest_preceeding_finger nr 2 error:", err)
					}
					n_temp = reply // set n_temp to be the closest preceeding node to the failed node

					client2, err2 = rpc.DialHTTP("tcp", n_temp.Ip+":"+n_temp.Port) // call the node that is preceeding the failed node
					if err2 != nil {
						log.Println("dialing nr1:", err2)
					}
					err2 = client2.Call("Node.GetSucc", Args{}, &reply) // get the successor that is still alive
					if err2 != nil {
						log.Println("GetSucc error:", err2)
					}
					err2 = client2.Close()
					if err2 != nil {
						log.Println("Closing conn error:", err2)
					}
					succ = reply
				}
			}
		} else { // connection established
			err2 = client2.Call("Node.GetSucc", Args{}, &reply)
			if err2 != nil {
				log.Println("GetSucc error:", err2)
			}
			err2 = client2.Close()
			if err2 != nil {
				log.Println("Closing conn error:", err2)
			}
			succ = reply
		}

		err = client.Close()
		if err != nil {
			log.Fatal("Closing conn error:", err)
		}
		// fmt.Println("Ny n_temp: ", n_temp.Id.String(), "och n_temp.succ: ", succ.Id.String())
		// fmt.Println("------------------------------------------------------------------------------------------")

	}
	fmt.Println("find_predecessor klar.")
	return n_temp
}

func (this *Node) Closest_preceeding_finger(arg Args, res *Bro) error {

	//locks the node until it has been modified
	// this.mutex.Lock()
	// defer this.mutex.Unlock()

	// fmt.Println("Inne i Closest_preceeding_finger")
	for i := m - 1; i >= 0; i-- { //Start with the finger which is last in the list (furthest away)
		// fmt.Println("Nu kör vi inbetween.")
		// fmt.Println("Nodeid: ", this.id.String(), "Finger nr: ", i+1)

		// fmt.Println("Ligger ", this.finger[i].succ.Id.String(), "mellan ", this.id.String(), "och ", arg.Key.String(), "?")
		if inbetween(this.id, arg.Key, this.finger[i].succ.Id) { // if the fingers successor lays between 'this' and 'key'
			// fmt.Println("Svar: Ja!")
			*res = *this.finger[i].succ //since we start with the finger farthest away, the first fingers successor must be the being closest to key
			return nil
		}
		// fmt.Println("Svar: Nej")
		// fmt.Println("Fingers successor-id: ", this.finger[i].succ.Id.String(), "start=", this.finger[i].start.String())
	}
	*res = this.nodeToBro()
	return nil
}

//finds the node laying immedietely before 'key', or just before 'key'
func (this *Node) findNodeWithFinger(key big.Int) *Bro {

	//locks the node until it has been modified
	this.mutex.Lock()
	defer this.mutex.Unlock()

	// fmt.Println("Inne i findNodeWithFinger.", key)
	pred := this.find_predecessor(key) //retrieve the node laying just before 'key'
	// fmt.Println("pred.id=", pred.Id.String())
	var predSucc Bro
	// BEGIN RPC ======================================
	if this.id.Cmp(&pred.Id) == 0 {
		predSucc = *this.succ
	} else {
		address := pred.Ip + ":" + pred.Port
		client, err := rpc.DialHTTP("tcp", address)
		if err != nil {
			log.Fatal("dialing:", err)
		}
		err = client.Call("Node.GetSucc", Args{}, &predSucc)
		if err != nil {
			log.Fatal("GetSucc error:", err)
		}
		err = client.Close()
		if err != nil {
			log.Fatal("Closing conn error:", err)
		}

		// END RPC ========================================
	}
	// fmt.Println("predSucc.id=", predSucc.Id.String())
	if predSucc.Id.Cmp(&key) == 0 { //fix this one with bigInt-comparison
		//if exist, the chance is high this very node will get its finger updated

		return &predSucc
	} else {
		return &pred
	}
}

//-------------------------end lookup----------------------------------------------------------------------------------------------------

// func (this *Node) LinLookup(key big.Int) *Bro {

// 	//locks the node until it has been modified
// 	this.mutex.Lock()
// 	defer this.mutex.Unlock()

// if betweenTo(this.id, this.succ.id, key) { //key's successor is in fact this.node's successor!
// 	return this.succ
// }
// } else {
// return nil
// }

// 	if this.id.Cmp(&key) == 0 { //key is located at this.node!
// 		tmp := this.nodeToBro()
// 		return &tmp
// 	}
// 	if this.succ != nil {

// 		if betweenTo(this.id, this.succ.Id, key) { //key's successor is in fact this.node's successor!
// 			//fmt.Println(this.succ)
// 			return this.succ
// 		}
// 	} else {
// 		return nil
// 	}

// 	//så här säger Chord-pappret, man går alltså counterclockwise rekursivt i fingertabellen.
// 	// forward the query around the circle...
// 	// n0 := closest_preceding_node(key);
// 	// return n0.lookup(key)
// 	res := this.succ.linLookup(key) //traversing the chord circle linearly right now
// 	fmt.Println("Per är 1337")

// 	return res
// }

//a new node n shall use some node n´ to initialize it's state and add itself to the existing Chord network.

// func (this *Node) testCalcFingers() bool {
// 	//locks the node until it has been modified
// 	this.mutex.Lock()
// 	defer this.mutex.Unlock()

// 	fmt.Println("testCalcFingersKörs.")
// 	for i := 0; i < m; i++ {
// 		start := this.linLookup(this.finger[i].start)
// 		if this.finger[i].succ.Id.Cmp(&start.id) != 0 { //Med en linjär lookup
// 			fmt.Println("Nod nummer: ", this.id.String(), "Finger fel nr:", i+1, "start=", this.finger[i].start.String(), "succ=", this.finger[i].succ.Id.String())
// 			fmt.Println("Rätt successor=", this.linLookup(this.finger[i].start).id.String())
// 			return false
// 		}
// 	}
// 	return true
// }

func (this *Node) initFingers(elder *Bro) {

	//locks the node until it has been modified
	this.mutex.Lock()
	defer this.mutex.Unlock()

	currentNode := this.nodeToBro()

	//this.finger = make([]*Finger, m)
	// fmt.Println("Sätter alla startvärden.")
	for i := 1; i <= m; i++ {
		//Firstly creates a new finger with a generated ID, and sets the finers succ = nil for each slot
		//in the finger table
		bt := calcFingerStart(this.id, i, m)
		this.finger[i-1] = &Finger{bt, &Bro{}}
	}
	var succ Bro
	address := elder.Ip + ":" + elder.Port
	client, err := rpc.DialHTTP("tcp", address)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	err = client.Call("Node.Lookup", Args{Key: this.finger[0].start}, &succ)
	if err != nil {
		log.Fatal("Lookup error:", err)
	}
	*this.finger[0].succ = succ

	//this.finger[0].succ = elder.lookup(this.finger[0].start) //the first finger is copied from the elders fingertable[0]
	// fmt.Println("First finger start value=", this.finger[0].start.String(), "succ=", this.finger[0].succ.Id.String())

	//Finally fills the fingertable with correct start and succ values

	fmt.Println("Set successors to all startvalues in fingertable.")
	for i := 0; i < m-1; i++ {
		// fmt.Println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&" + strconv.Itoa(i))
		if betweenTo(currentNode.Id, this.finger[i].succ.Id, this.finger[i+1].start) {
			*this.finger[i+1].succ = *this.finger[i].succ
			// fmt.Println("YES fingret med start", this.finger[i+1].start.String(), " låg mellan", currentNode.Id.String(), "och", this.finger[i].succ.Id.String())

		} else {
			err = client.Call("Node.Lookup", Args{Key: this.finger[i+1].start}, &succ)
			*this.finger[i+1].succ = succ
			//this.finger[i+1].succ = elder.lookup(this.finger[i+1].start)
			// fmt.Println("NO Fingret med start", this.finger[i+1].start.String(), " låg inte mellan", currentNode.Id.String(), "och ", this.finger[i].succ.Id.String())
			currentNode = *this.finger[i].succ
			// fmt.Println("Ny currentNode satt. Intervallets nedre gräns är nu nod: ", currentNode.Id.String())
		}
		// fmt.Println("finger[", i+2, "].succ =", this.finger[i+1].succ.Id.String())
	}
	err = client.Close()
	if err != nil {
		log.Fatal("Closing conn error:", err)
	}
}

func (this *Node) FirstInitFingers(arg Args, out *string) error {
	//this.finger = make([]*Finger, m)

	for i := 1; i <= m; i++ {
		bt := calcFingerStart(this.id, i, m)
		this.finger[i-1] = &Finger{bt, &Bro{}}
		//Node 1 only enters the if-statement. Node only enters the else-statement, no matter start-values
		// fmt.Println("Check firstInitFinger:")
		// fmt.Println("Startnods id: ", this.id.String(), "Startvärde: ", this.finger[i-1].start.String(), "Elder id: ", arg.S.Id.String())
		if betweenTo(this.id, arg.S.Id, bt) { //if the start value lays after this node but before the second node
			*this.finger[i-1].succ = arg.S //the second node will become successor to the finger
			// fmt.Println(this.finger[i-1].start.String(), "ligger mellan ", this.id.String(), "och", arg.S.Id.String())
			// fmt.Println("Successor till startvärdet ska vara: ", arg.S.Id.String())
			// fmt.Println("Successor är: ", this.finger[i-1].succ.Id.String())
		} else { //else the start value lays after the second node
			//this.finger[i-1].succ = this //then, the succesor to the finger will become the same as the one we initialize the fingertable for
			*this.finger[i-1].succ = this.nodeToBro()
			// fmt.Println(this.finger[i-1].start.String(), "ligger inte mellan ", this.id.String(), "och", arg.S.Id.String())
			// fmt.Println("Successor till startvärdet ska vara: ", this.id.String())
			// fmt.Println("Successor är: ", this.finger[i-1].succ.Id.String())
		}
	}

	// Initialize the successor list
	for i := 0; i < r; i++ {
		this.succList[i] = &Bro{}
	}
	*this.succList[0] = arg.S
	return nil
}

func (this *Node) updateOthers() {
	fmt.Println("Uppdaterar andra noder")
	// fmt.Println("Nuvarande nods id: ", this.id.String())
	thisAsBro := this.nodeToBro()
	//var reply *Bro

	for i := 1; i <= m; i++ {
		// fmt.Println("Nod: ", this.id.Bytes(), "   Finger nr: ", i)
		dist := subDistance(this.id, i-1, m) ////calculate a position on the ring which lays 2^i-1 steps earlier on the ring
		// fmt.Println("Dist beräknat till: ", dist.Bytes())
		//we ought to find a node which should contain a finger, whos successor most likely is the new node
		// fmt.Println("Denna nods id är: ", this.id.String())
		// fmt.Println("Denna nods successor är: ", this.succ.Id.String())
		// fmt.Println("Distansen till föregående nod är: dist = ", dist.String())

		p := this.findNodeWithFinger(dist)
		// fmt.Println("Noden somligger före", dist.String(), "är", p.Id.String())
		// BEGIN RPC ======================================
		var out string
		address := p.Ip + ":" + p.Port
		client, err := rpc.DialHTTP("tcp", address)
		if err != nil {
			log.Fatal("dialing:", err)
		}
		// fmt.Println("anslutit till den som ska uppdateras")
		err = client.Call("Node.UpdateFingerTable", Args{S: thisAsBro, I: i}, &out)
		if err != nil {
			log.Fatal("UpdateFingerTable error:", err)
		}
		fmt.Println("lyckats uppdatera")
		err = client.Close()
		if err != nil {
			log.Fatal("Closing conn error:", err)
		}
		// END RPC ========================================
		//p.updateFingerTable(this, i) //update 'this' finger number i
	}
	//find the node which lays 2^i-1 steps before 'this' on the ring

}

func (this *Node) UpdateFingerTable(arg Args, out *string) error {
	//locks the node until it has been modified
	this.mutex.Lock()
	//defer this.mutex.Unlock()

	// fmt.Println("\nUppdaterar ett finger på denna Node:", this.id.String(), "finger nr", arg.I)
	// fmt.Printf("Finger %d --> Start: %d  Successor: %d\n", arg.I, this.finger[arg.I-1].start.String(), this.finger[arg.I-1].succ.Id.String())

	//check that the new node really lays between the fingers start value and its successor
	if betweenFrom(this.finger[arg.I-1].start, this.finger[arg.I-1].succ.Id, arg.S.Id) { // s E (finger[i].start,finger[i].node]
		//if inbetween(this.id, this.finger[i-1].succ.Id, s.id) {
		*this.finger[arg.I-1].succ = arg.S
		p := this.pred
		// fmt.Println("uppdaterade fingret. Predecessor:", p.Id.String())
		*out = "satte finger nr" + strconv.Itoa(arg.I) + "med start" + this.finger[arg.I-1].start.String() + "till nod" + arg.S.Id.String()
		// fmt.Println(*out)
		this.mutex.Unlock()
		// BEGIN RPC ======================================
		var out2 string
		address := p.Ip + ":" + p.Port
		client, err := rpc.DialHTTP("tcp", address)
		if err != nil {
			log.Fatal("dialing:", err)
		}
		// fmt.Println("NÄSTA anslutit", address)
		err = client.Call("Node.UpdateFingerTable", arg, &out2)
		if err != nil {
			log.Fatal("UpdateFingerTable error:", err)
		}
		// fmt.Println("NÄSTA Uppdaterad")
		err = client.Close()
		if err != nil {
			log.Fatal("Closing conn error:", err)
		}
		// fmt.Println("NÄSTA closed")
		//*out = *out + out2
		// END RPC ========================================
		//p.updateFingerTable(arg.S, arg.I) //check the predecessor if it also needs to change its finger number i
		return nil

	} else {
		// fmt.Println("ej uppdaterat fingret")
		// fmt.Println("EJ ändrat finger", arg.I, "med start", this.finger[arg.I-1].start.String())
		*out = "EJ ändrat finger" + strconv.Itoa(arg.I) + "med start" + this.finger[arg.I-1].start.String()
		this.mutex.Unlock()
		return nil
	}
	return nil
}

func (this *Node) GetSucc(arg Args, succ *Bro) error {
	//locks the node until it has been modified
	this.mutex.Lock()
	defer this.mutex.Unlock()
	if !this.succ.Empty() { // not un-initialized
		if this.succ.alive() { // the closest successor is contactable
			*succ = *this.succ
			return nil
		} else { // Successor is dead, find and return the next successor
			*this.succ = Bro{}
			var cursor int = 0 // cursor used for moving the items in the successor list forward.
			for i := 1; i < r; i++ {
				if this.succList[i].Empty() {
					// No successor found
				} else if succ.Empty() { // havent found the new successor
					if this.succList[i].alive() { //
						*this.succ = *this.succList[i]
						*this.succList[cursor] = *this.succList[i]
						cursor++
						*succ = *this.succ

					} else {
					}
				} else { // move the rest of the successor to the front
					*this.succList[cursor] = *this.succList[i]
					this.succList[i] = &Bro{}
					cursor++
				}
			}
			if succ.Empty() {
				return errors.New("No successor found")
			}
			return nil

		}
	}
	return errors.New("No successor found")
}

// RPC Function that handles requests to get this nodes list of successors. Called by the function that creates the succList.
func (this *Node) GetSucclist(arg Args, list *[r]Bro) error {
	for i := 0; i < r; i++ { // loop through the list
		list[i] = *this.succList[i] //Fill the result parameter.
	}
	return nil
}

// RPC Function that handles requests to get this nodes predecessor.
func (this *Node) GetPred(arg Args, pred *Bro) error {
	if this.pred.Empty() {
		return errors.New("No predecessor found")
	} else if this.pred.alive() == false {
		return errors.New("Predecessor is dead")
	} else {
		*pred = *(this.pred)
	}
	return nil
}

// RPC Function for handeling discovery requests. Sends back the requested information about this node.
func (this *Node) DiscoverResponder(arg Args, resp *Bro) error {
	*resp = this.nodeToBro()
	return nil
}

//En funktion för att uppdatera predecessor hos en nod
func (this *Node) Notify(arg Args, out *string) error {
	//locks the node until it has been modified
	fmt.Println("Notify args:", arg)

	this.mutex.Lock()
	if this.pred.Empty() { // 'this' does not have any predecessor from before
		*this.pred = arg.S
		if this.succ.Empty() { // also check whether the node has a successor
			//if not, the node is one of the two first nodes in the ring
			*this.succ = arg.S // set predecessor to the same node as the predecessor
			fmt.Println("Notify set successor")
		}
	} else if this.pred.alive() {
		if inbetween(this.pred.Id, this.id, arg.S.Id) { //check whether 's' lays closer to 'this' than 'this.pred'
			this.pred = &arg.S
		}
	} else { // our predecessor is dead, take whatever we get
		this.pred = &arg.S
	}
	this.mutex.Unlock()
	return nil
}

//every node should run this periodically in the background to maintain correct reachability to all
//other nodes this is how newly joined nodes are noticed by the network
func (this *Node) stabilize() {

	//locks the node until it has been modified

	//first stabilize the succList
	this.stabilizeSuccList()

	// Get our successors predecessor
	var succPred Bro //RPC return value
	client, err := rpc.DialHTTP("tcp", this.succ.Ip+":"+this.succ.Port)
	if err != nil { // the successor is dead, get a new one
		log.Println("stabilize dialing:", err)
	} else { // successor is still alive
		err = client.Call("Node.GetPred", Args{}, &succPred)
		if err != nil {
			log.Println("stabilize GetSucc error:", err)
		}
		err = client.Close()
		if err != nil {
			log.Fatal("stabilize Closing conn error:", err)
		}
		if succPred.Empty() == false { // if we found a predecessor
			this.mutex.Lock()
			if inbetween(this.id, this.succ.Id, succPred.Id) { //if x lays between this and this.succ, that would mean x is a newly joined node
				fmt.Println("stabilize Changing successor from", this.succ, "\nto:", succPred)
				*this.succ = succPred //and this successor is thus set to be x
				this.succ.Id.Set(&succPred.Id)
				this.succ.Ip = succPred.Ip
				this.succ.Port = succPred.Port
				this.replicate_db(this.succ) // replicate the data to the new successor
			}
			this.mutex.Unlock()
		}
		// Notify our successor that it has a new predecessor
		var out string
		client2, err2 := rpc.DialHTTP("tcp", this.succ.Ip+":"+this.succ.Port)
		if err2 != nil {
			log.Fatal("stabilize dialing:", err2)
		}
		err2 = client2.Call("Node.Notify", Args{S: this.nodeToBro()}, &out)
		if err2 != nil {
			log.Fatal("stabilize Notify error:", err2)
		}
		err2 = client2.Close()
		if err2 != nil {
			log.Fatal("stabilize Closing conn error:", err2)
		}
	}
	// this asks it's successor for it's predeccessor

}

func (this *Node) stabilizeSuccList() {

	//locks the node until it has been modified
	this.mutex.Lock()
	var prevList [r]Bro // List of the nodes that were previously in the succList
	for i := 0; i < r; i++ {
		//prevList[i] = *this.succList[i]
		prevList[i].Id.Set(&this.succList[i].Id)
		prevList[i].Ip = this.succList[i].Ip
		prevList[i].Port = this.succList[i].Port
	}

	//if this.successor can't be found or failed for some reason
	if this.succ.alive() == false { // if the successor is dead
		fmt.Println("stabilizesuccList: dead successor")
		this.succ = &Bro{} // indicate that by setting it empty
		//this.succ.Id = *big.NewInt(1)
	}
	this.mutex.Unlock()
	if this.succ.Empty() { // no current closes successor set
		for n := 0; n < r; n++ {
			//If we find a node that is alive in the succList, we want to set that node to be the new successor, and copy it's
			//succList except for it's last element.
			if !this.succList[n].Empty() {
				//set the first alive node found, to this.succ
				var temp Bro
				client, err := rpc.DialHTTP("tcp", this.succList[n].Ip+":"+this.succList[n].Port) // call the node and see if it is alive
				if err != nil {
					continue // skip to the next iteration of the loop
					// to check the next node in the list
				}
				this.mutex.Lock()
				if this.succ.Empty() { // if no successor has been set
					fmt.Println("stabilize Changing successor from", this.succ, "\nto:", this.succList[n])
					//*this.succ = *this.succList[n] // set the successor to the first alive node in the successor list
					this.succ.Id.Set(&this.succList[n].Id)
					this.succ.Ip = this.succList[n].Ip
					this.succ.Port = this.succList[n].Port
				}
				this.mutex.Unlock()
				// temporary succList with the list that shall be copied from first til but not including the last element
				temp = Bro{}
				temp.Id = *big.NewInt(1)
				err = client.Call("Node.GetSucc", Args{}, &temp) // get the successor of the new closest successor we found
				if err != nil {
					log.Fatal("stabilizesuccList: dead Get first Succ error:", err)
				}
				err = client.Close()
				if err != nil {
					log.Fatal("Closing error")
				}
				fmt.Println("stabilizesuccList: successor of our new successor:", temp)
				//first element in this.succList shall be it's successor
				this.mutex.Lock()
				//*this.succList[0] = *this.succ
				this.succList[0].Id.Set(&temp.Id)
				this.succList[0].Ip = temp.Ip
				this.succList[0].Port = temp.Port
				this.mutex.Unlock()
				//fill the rest of this.succList with the nodes closest to us.
				for i := 1; i < r; i++ {
					var known bool = false // Variable that repressents if the successor already was in the successor list
					for _, a_succ := range prevList {
						known = a_succ.Id.Cmp(&temp.Id) == 0 // compare the successor to the ones in the previous succList
						if known {
							break
						}
					}
					this.mutex.Lock()
					//*this.succList[i] = temp
					this.succList[i].Id.Set(&temp.Id)
					this.succList[i].Ip = temp.Ip
					this.succList[i].Port = temp.Port
					this.mutex.Unlock()
					if !known { // In the case of a new successor
						this.replicate_db(this.succList[i])
					}
					client, err = rpc.DialHTTP("tcp", this.succList[i].Ip+":"+this.succList[i].Port) // Get the next successor in line
					if err != nil {
						log.Println("StabelizeSuccList: dead loop could not connect to successor", err)
						// to check the next node in the list
					}
					temp = Bro{}
					temp.Id = *big.NewInt(1)
					err = client.Call("Node.GetSucc", Args{}, &temp) // get the successor of the successor we found
					if err != nil {
						log.Fatal("stabilizesuccList: dead GetSucc loop error:", err)
					}
					err = client.Close()
					if err != nil {
						log.Fatal("Closing error")
					}
					if temp.Id.Cmp(&this.id) == 0 { // we have gone a full circle
						if i+1 == r { // we have reached the end of the list

						} else {
							this.mutex.Lock()
							for mo := i + 1; mo < r; mo++ { // fill the rest of the succList with empty nodes
								this.succList[mo] = &Bro{}
								this.succList[mo].Id = *big.NewInt(0)
							}
							this.mutex.Unlock()
						}
						break
					}
				}
				//the successorList is updated, so we break the for loop.
				break
			}
		}
	} else { // ALIVE
		//first element in this.succList shall be it's successor
		this.mutex.Lock()
		var temp Bro = *this.succ
		fmt.Println("temp = *this.succ:", temp)
		this.mutex.Unlock()
		//fill the rest of this.succList with the nodes closest to us.
		for i := 0; i < r; i++ {
			// check if the node was already in the list
			var known bool = false // Variable that repressents if the successor already was in the successor list
			for _, a_succ := range prevList {
				known = a_succ.Id.Cmp(&temp.Id) == 0 // compare the successor to the ones in the previous succList
				if known {
					break
				}
			}
			this.mutex.Lock()
			// fmt.Println("BEFORE this.succList[", i, "] = ", this.succList[i])
			// fmt.Println("BEFORE DEREFERENCE *this.succList[", i, "] = ", *this.succList[i])
			this.succList[i].Id.Set(&temp.Id)
			this.succList[i].Ip = temp.Ip
			this.succList[i].Port = temp.Port
			// fmt.Println("AFTER this.succList[", i, "] = ", this.succList[i])
			// fmt.Println("AFTER DEREFERENCE *this.succList[", i, "] = ", *this.succList[i])
			this.mutex.Unlock()
			if !known { // In the case of a new successor
				this.replicate_db(this.succList[i])
			}
			client, err := rpc.DialHTTP("tcp", this.succList[i].Ip+":"+this.succList[i].Port) // Get the next successor in line
			if err != nil {
				log.Println("StabelizeSuccList:  alive could not connect to successor", err)
				if this.succList[i].Empty() {
					break
				}
				this.mutex.Lock()
				if i+1 == r { // we have reached the end of the list
					this.succList[r-1] = &Bro{} // set the last elemnt to empty
				} else {
					for mo := i + 1; mo < r; mo++ { // move the rest of the list forward one step
						this.succList[mo-1] = this.succList[mo]
					}
					this.succList[r-1] = &Bro{} // set the last elemnt to empty
				}
				// fmt.Println("The succList after move")
				// for mo := 0; mo < r; mo++ { // fill the rest of the succList with empty nodes
				// fmt.Println(this.succList[mo])
				// }
				this.mutex.Unlock()
				i--      // move the curser back one step, as we removed an element from the list
				continue // to check the next node in the list
			}
			temp = Bro{}
			temp.Id = *big.NewInt(1)
			err = client.Call("Node.GetSucc", Args{}, &temp) // get the successor of the successor in the succList
			if err != nil {
				log.Fatal("stabilizesuccList: alive GetSucc loop error:", err)
			}
			err = client.Close()
			if err != nil {
				log.Fatal("stabilizesuccList: Closing error")
			}
			// fmt.Println("stabilizesuccList: alive loop, temp:", temp)
			// fmt.Println("TEMP this.succList[", i, "] = ", this.succList[i])
			// fmt.Println("TEMP DEREFERENCE *this.succList[", i, "] = ", *this.succList[i])
			if temp.Id.Cmp(&this.id) == 0 { // we have gone a full circle
				fmt.Println("full circle")
				if i+1 == r { // we have reached the end of the list

				} else {
					this.mutex.Lock()
					for mo := i + 1; mo < r; mo++ { // fill the rest of the succList with empty nodes
						this.succList[mo] = &Bro{}
						this.succList[mo].Id = *big.NewInt(0)
					}
					this.mutex.Unlock()
				}
				break
			}
		}
		//the successorList is updated
	}
}

//takes random index i from this fingertable and corrects it (whether it's already correct or not)
//this function should, for each Node in the ring, be called periodically to maintain correctness
//in the fingertables
func (this *Node) fixFinger() {
	i := randomInt(m) //a random index in the finger table
	var newsucc Bro
	this.Lookup(Args{Key: this.finger[i].start}, &newsucc)
	this.mutex.Lock()
	*this.finger[i].succ = newsucc //sets the successor of finger i to it's correct value
	this.mutex.Unlock()
}

//generates a random int for fixFinger
func randomInt(max int) int { return rand.Intn(max) }

//runs when a Node joins the network
func (this *Node) createSuccessorList() {
	//create a successor list of size r
	fmt.Println("in create successor list")
	//locks the node until it has been modified
	this.mutex.Lock()
	fmt.Println("create successor list lock taken")
	for i := 0; i < r; i++ {
		this.succList[i] = &Bro{}
	}

	var temp_succ Bro = *this.succ
	for i := 0; i < r; i++ {
		//*this.succList[i] = temp_succ
		this.succList[i].Id.Set(&temp_succ.Id)
		this.succList[i].Ip = temp_succ.Ip
		this.succList[i].Port = temp_succ.Port

		// Get the next successor
		client2, err2 := rpc.DialHTTP("tcp", this.succList[i].Ip+":"+this.succList[i].Port)
		if err2 != nil {
			log.Fatal("dialing:", err2)
		}
		temp_succ = Bro{}
		temp_succ.Id = *big.NewInt(1)
		err2 = client2.Call("Node.GetSucc", Args{}, &temp_succ)
		if err2 != nil {
			log.Fatal("GetSucc error:", err2)
		}
		err2 = client2.Close()
		if err2 != nil {
			log.Fatal("Closing conn error:", err2)
		}
		if temp_succ.Id.Cmp(&this.id) == 0 { // We have circled the ring back to our self
			break
		}
	}
	this.mutex.Unlock()
	fmt.Println("create successor list lock released")
}

func Main() {
	// ###########################################################
	// Parameters for the First node
	// Uncomment when atempting to build the stand alone executable
	// Build command (Powershell): go build -o first.exe -i main.go
	//		-¤-Observe that you must be standing in the same folder as main.go
	//		   The compiled file will be put in the same folder.
	// ###########################################################
	// id1 := big.NewInt(1)
	// node1 := makeDHTNode(*id1, "127.0.0.1", "1111")
	// go node1.listen() // Start listening for RPC requests

	// ###########################################################
	// Parameters for the Second node
	// Uncomment when just running it in a powershell window
	// run command (Powershell): go run main.go
	//		-¤-Observe that you must be standing in the same folder as main.go
	// ###########################################################
	//id1 := big.NewInt(124536923712241)
	var node1 *Node

	fmt.Println(os.Args)
	if len(os.Args) > 3 {
		if os.Args[2] != "0" && os.Args[3] != "0" {
			node1 = makeDHTNode(nil, os.Args[2], os.Args[3])
			glob_ip = os.Args[2]
			glob_port = os.Args[3]
		} else {
			node1 = makeDHTNode(nil, "127.0.0.1", "1111")
			glob_ip = "127.0.0.1"
			glob_port = "1111"
		}
	} else {
		node1 = makeDHTNode(nil, "127.0.0.1", "1111")
		glob_ip = "127.0.0.1"
		glob_port = "1111"
	}
	fmt.Println("Local node", node1.nodeToBro())
	go node1.listen() // Start listening for RPC requests
	if len(os.Args) > 5 {
		if os.Args[4] != "0" && os.Args[5] != "0" {
			gran := node1.discoverBro(os.Args[4], os.Args[5]) //Get another node in the ring
			node1.addToRing(&gran)                            // Attach this node to the ring, via the gran node
		}

	}

	//fmt.Println("lokal Lookup returnerar: ", node1.Lookup(Args{Key: *big.NewInt(111111)}, &gran))
	node1.initDB()
	go runServer()
	//defer node1.close_db()
	// var input chan string
	// go func() {
	// 	for {
	// 		var i string
	// 		_, err := fmt.Scanf("%s", &i)
	// 		fmt.Println("lomk", i)
	// 		if err != nil {
	// 			return
	// 		}
	// 		input <- i
	// 	}
	// }()
	//reader := bufio.NewReader(os.Stdin)
	// for {
	// 	var i string
	// 	_, err := fmt.Scanf("%s", &i)
	// 	switch i {
	// 	case "insert":
	// 		var key, val string
	// 		fmt.Println("key:")
	// 		_, err := fmt.Scanf("%s", &key)
	// 		if err != nil {
	// 			fmt.Println(err.Error())
	// 		}
	// 		fmt.Println("val:")
	// 		_, err2 := fmt.Scanf("%s", &val)
	// 		if err2 != nil {
	// 			fmt.Println(err2.Error())
	// 		}
	// 		fmt.Println("insert")
	// 		tran := node1.put_db(Data{Key: key, Value: val})
	// 		fmt.Println(<-tran)
	// 	case "fetch":
	// 		var key string
	// 		_, err := fmt.Scanf("%s", &key)
	// 		if err != nil {
	// 			fmt.Println(err.Error())
	// 		}
	// 		tran := node1.get_db(Data{Key: key})
	// 		fmt.Println(<-tran)
	// 	case "addtoring":
	// 		// var key string
	// 		// _, err := fmt.Scanf("%s", &key)
	// 		// if err != nil {
	// 		// 	fmt.Println(err.Error())
	// 		// }
	// 		// tran := node1.fetch(Data{Key: []byte(key)})
	// 		// fmt.Println(<-tran)
	// 	case "quit":
	// 		break
	// 	}
	// 	if err != nil {
	// 		fmt.Println(err.Error())
	// 	}
	// }

	//is ran asynchronously, to handle the concurrency of all function calls being made to the nodes in the network
	go func() {
		//executes every 3 seconds
		c := time.Tick(10 * time.Second)
		for now := range c {
			fmt.Println("succList:")
			for i := 0; i < r; i++ {
				fmt.Println(node1.succList[i])
			}
			fmt.Println(now)
			node1.stabilize()
			//node1.fixFinger()

		}
	}()
	//listen for requests and establishes TCP-connections
	//go this.listen()
}
