package dht

import (
	_ "crypto/sha1"
	"fmt"
	"github.com/boltdb/bolt"
	_ "github.com/nu7hatch/gouuid"
	"log"
	"math/big"
	"net/rpc"
)

type Data struct {
	Parent big.Int // The id of the responsible node.
	User   string
	Key    string
	Value  string
}

type Res struct {
	Succ int // 1 if operation succeed, 0 if it fails
	Ds   Data
	Err  error
}

/////////////////////////////////////////////////////////////////////////////
// Handle all storage of values
var dht_store_err string = "dht_store: "

func fanin(input1, input2 <-chan Res) <-chan Res { // taken from and modified:https://talks.golang.org/2012/concurrency.slide#27
	var out chan Res
	go func() { out <- <-input1 }() // forward output only once
	go func() { out <- <-input2 }() // forward output only once
	return out
}

func (this *Node) initDB() {
	var err error
	this.db, err = bolt.Open(this.port+"my.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	err = this.db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(this.id.Bytes())
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		log.Fatal("Local bucket error:", err)
	}
}

func (this *Node) close_db() {
	this.db.Close()
}

func (this *Node) delete_bucket(bucketName []byte) <-chan Res {
	c := make(chan Res)
	go func() {
		err := this.db.Update(func(tx *bolt.Tx) error {
			err := tx.DeleteBucket(bucketName)
			if err != nil {
				return fmt.Errorf("Could not delete bucket:", err)
			}
			fmt.Printf("bucket %s deleted", bucketName)
			return nil

		})
		if err != nil {
			c <- Res{Succ: 0, Err: err}
		} else {
			c <- Res{Succ: 1}
		}
	}()
	return c
}

func (this *Node) replicate_db(replicant *Bro) <-chan Res {
	c := make(chan Res)
	go func() { // put it up as a indipendent goroutine, thus can it replicate to multiple targets in paralell
		err := this.db.View(func(tx *bolt.Tx) error {
			b := tx.Bucket(this.id.Bytes()) // fetch the local bucket
			if b == nil {
				return fmt.Errorf("Bucket %q not found!", this.id.Bytes())
			}

			var result Res //RPC return value
			client, err := rpc.DialHTTP("tcp", replicant.Ip+":"+replicant.Port)
			if err != nil {
				return fmt.Errorf("Replicate_db"+"dialing:", err)
			}

			err = b.ForEach(func(k, v []byte) error { // loop through each value in the local bucket
				err = client.Call("Node.PutReplicateData", Data{Parent: this.id, Key: string(k), Value: string(v)}, &result) // put this data on another node.
				if err != nil || result.Err != nil {
					if result.Err.Error() == "Key allready exist in the database" { // if the entry already existed at the replicant, do nothing as it allready exsists
						fmt.Println("REPLIKERING: nyckeln fanns redan")
						return nil
					}
					return fmt.Errorf("Replicate_db"+"PutData error:", err, result.Err)
				}
				return nil
			})
			if err != nil {
				return err
			}

			err = client.Close()
			if err != nil {
				return fmt.Errorf("Replicate_db"+"closing error:", err)
			}
			return nil
		})
		if err != nil {
			c <- Res{Succ: 0, Err: err}
		} else {
			c <- Res{Succ: 1}
		}
	}()

	return c
}
func (this *Node) put_db(arg Data) <-chan Res {
	c := make(chan Res)
	go func() {
		err := this.db.Update(func(tx *bolt.Tx) error {

			bucket, err := tx.CreateBucketIfNotExists(arg.Parent.Bytes())
			if err != nil {
				return err
			}
			fmt.Println("put_db: hämtat bucket")
			val := bucket.Get([]byte(arg.Key))
			if val != nil {
				fmt.Println("det fanns redan ett value")
				return fmt.Errorf("Key allready exist in the database")
			}
			err = bucket.Put([]byte(arg.Key), []byte(arg.Value))
			if err != nil {
				return err
			}
			fmt.Println("put_db: sparat data")
			c <- Res{Succ: 1}
			return nil
		})
		if err != nil {
			c <- Res{Succ: 0, Err: err}
		} else {
			//c <- Res{Succ: 1}
		}
	}()

	return c
}
func (this *Node) mod_db(arg Data) <-chan Res {
	c := make(chan Res)
	go func() {
		err := this.db.Update(func(tx *bolt.Tx) error {

			bucket, err := tx.CreateBucketIfNotExists(arg.Parent.Bytes())
			if err != nil {
				return err
			}
			fmt.Println("mod_db: hämtat bucket")
			err = bucket.Put([]byte(arg.Key), []byte(arg.Value))
			if err != nil {
				return err
			}
			fmt.Println("put_db: sparat data")
			c <- Res{Succ: 1}
			return nil
		})
		if err != nil {
			fmt.Println("put_db: err", err)
			c <- Res{Succ: 0, Err: err}
		} else {
			//c <- Res{Succ: 1}
		}
	}()

	return c
}
func (this *Node) get_db(arg Data) <-chan Res {
	c := make(chan Res)
	go func() {
		var val []byte
		err := this.db.View(func(tx *bolt.Tx) error {
			bucket := tx.Bucket(arg.Parent.Bytes())
			if bucket == nil {
				return fmt.Errorf("Bucket %q not found!", arg.Parent.Bytes())
			}

			val = bucket.Get([]byte(arg.Key))
			fmt.Println(string(val))

			return nil
		})
		if val == nil { // Could not find the value
			fmt.Println("hittade inte värdet")
			err = this.db.Update(func(tx *bolt.Tx) error {
				var nameOfBucket []byte
				tx.ForEach(func(name []byte, b *bolt.Bucket) error {
					retrived := b.Get([]byte(arg.Key)) // try an retrieve the value from each bucket
					if retrived != nil {               // we found the value
						val = retrived
						nameOfBucket = name // retrieve the name of the bucket
						return nil
					}
					return nil

				})
				if val == nil { // no value found in any bucket
					return fmt.Errorf("Value not found!")
				}

				// Assume that the original responsible node i dead
				// Absorb all of its values into the local bucket
				deadBucket := tx.Bucket(nameOfBucket)     // get the bucket that represents the dead node
				localBucket := tx.Bucket(this.id.Bytes()) // get the local bucket
				deadBucket.ForEach(func(k, v []byte) error {
					localBucket.Put(k, v) // store the value local bucket
					return nil
				})
				delerr := tx.DeleteBucket(nameOfBucket)
				if delerr != nil {
					return fmt.Errorf("Unable to delete bucket!", delerr)
				}
				//Connect to the replicants (successors in succList) and tell them to delete the same bucket
				// var result Res //RPC return value
				// for i := 0; i < r; i++ {
				// 	if this.succList[i].Empty() == false { // If there is an entry in the succlist
				// 		client, err := rpc.DialHTTP("tcp", this.succList[i].Ip+":"+this.succList[i].Port)
				// 		if err != nil {
				// 			return fmt.Errorf("get_db"+"dialing replicants:", err)
				// 		}
				// 		err = client.Call("Node.DeleteDeadBucket", nameOfBucket, &result) // Tell the replicant to delete the dead nodes bucket.
				// 		if err != nil || result.Err != nil {
				// 			return fmt.Errorf("get_db"+"DeleteDeadBucket error:", err, result.Err)
				// 		}
				// 		err = client.Close()
				// 		if err != nil {
				// 			return fmt.Errorf("get_db"+"closing replicants error:", err)
				// 		}
				// 	}
				// }

				c <- Res{Succ: 2,
					Ds: Data{
						Parent: *big.NewInt(0).SetBytes(nameOfBucket), // send back the id of the dead node, hence the name of the dead bucket bucket
						Key:    arg.Key,
						Value:  string(val)}} // indicate that the data was absorbed
				return nil
			})
			if err != nil {
				c <- Res{Succ: 0, Err: err}
			}

		} else {
			c <- Res{Succ: 1, Ds: Data{Key: arg.Key, Value: string(val)}}
		}

	}()
	return c
}

// ------- web functions for rpc calls

func (this *Node) Web_lookup(inp Data, out *Res) error {
	var holder Bro
	var result Res //RPC return value
	hash := inp.hashit()
	this.Lookup(Args{Key: hash}, &holder)
	data := Data{Parent: holder.Id, Key: inp.Key}
	fmt.Println("Hittade ansvarig")

	if holder.Id.Cmp(&this.id) == 0 { // This node is responsible for Key
		fmt.Println("lokalt ska det vara")
		err := this.GetData(data, &result)
		if err != nil {
			return err
		}
		*out = result
	} else {
		var result Res //RPC return value
		client, err := rpc.DialHTTP("tcp", holder.Ip+":"+holder.Port)
		if err != nil {
			return err
			log.Panic(dht_store_err+"dialing:", err)
		}
		err = client.Call("Node.GetData", data, &result) // put this data on another node.
		if err != nil {
			return err
			log.Panic(dht_store_err+"GetData error:", err)
		}
		err = client.Close()
		if err != nil {
			log.Panic(dht_store_err+"closing error:", err)
		}
		*out = result
	}
	out.Ds.Key = holder.Port // DIRTY WAY: send the port of the responsible node back
	return nil
}

func (this *Node) Web_send(inp Data, out *Res) error {
	var holder Bro
	var result Res //RPC return value
	hash := inp.hashit()
	this.Lookup(Args{Key: hash}, &holder)
	data := Data{Parent: holder.Id, Key: inp.Key, Value: inp.Value}
	fmt.Println("Hittade ansvarig", holder)
	if holder.Id.Cmp(&this.id) == 0 { // This node is responsible for Key
		fmt.Println("lokalt ska det vara")
		err := this.PutData(data, &result)
		if err != nil {
			return err
		}
		*out = result

	} else {

		client, err := rpc.DialHTTP("tcp", holder.Ip+":"+holder.Port)
		if err != nil {
			return err
			log.Panic(dht_store_err+"dialing:", err)
		}
		err = client.Call("Node.PutData", data, &result) // put this data on another node.
		if err != nil {
			fmt.Println("fel med Node.PutData")
			return err
			log.Panic(dht_store_err+"PutData error:", err)
		}
		fmt.Println("klar med att lagra")
		err = client.Close()
		if err != nil {
			log.Panic(dht_store_err+"closing error:", err)
		}
		*out = result
	}
	out.Ds.Key = holder.Port // DIRTY WAY: send the port of the responsible node back
	return nil

}

func (this *Node) Web_update(inp Data, out *Res) error {
	var holder Bro
	var result Res //RPC return value
	hash := inp.hashit()
	this.Lookup(Args{Key: hash}, &holder)
	data := Data{Parent: holder.Id, Key: inp.Key, Value: inp.Value}
	fmt.Println("Hittade ansvarig", holder)
	if holder.Id.Cmp(&this.id) == 0 { // This node is responsible for Key
		fmt.Println("lokalt ska det vara")
		err := this.ModData(data, &result)
		if err != nil {
			return err
		}
		*out = result
	} else { // some other node is responsible for the Key
		client, err := rpc.DialHTTP("tcp", holder.Ip+":"+holder.Port)
		if err != nil {
			return err
			log.Panic(dht_store_err+"dialing:", err)
		}
		err = client.Call("Node.ModData", data, &result) // Mod the entry that is on another node
		if err != nil {
			return err
			log.Panic(dht_store_err+"PutData error:", err)
		}
		err = client.Close()
		if err != nil {
			log.Panic(dht_store_err+"closing error:", err)
		}
		*out = result
	}
	return nil

}

//---- functions that other nodes call via RPC ------------------

func (this *Node) GetData(arg Data, ret *Res) error {
	c := this.get_db(arg)
	*ret = <-c
	if ret.Succ == 2 { // The original node was dead, this node is now responsible for the keys
		// Spread the changes made to the local bucket, to the replicants
		this.mutex.Lock()
		defer this.mutex.Unlock()

		for i := 0; i < r; i++ { // loop through the succList
			if this.succList[i].Empty() == false { // If there is an entry in the succlist
				go func(nm int) { // start it as an indipendent function
					var result Res                            //RPC return value
					c := this.replicate_db(this.succList[nm]) // replicate the entire local bucket, to propagate the change
					p := <-c                                  // wait for replication to finish, before deleting the dead nodes bucket
					fmt.Println(p)
					client, err := rpc.DialHTTP("tcp", this.succList[nm].Ip+":"+this.succList[nm].Port)
					if err != nil {
						fmt.Errorf("get_db"+"dialing replicants:", err)
					}
					err = client.Call("Node.DeleteDeadBucket", ret.Ds.Parent.Bytes(), &result) // Tell the replicant to delete the dead nodes bucket.
					if err != nil || result.Err != nil {
						fmt.Errorf("get_db"+"DeleteDeadBucket error:", err, result.Err)
					}
					err = client.Close()
					if err != nil {
						fmt.Errorf("get_db"+"closing replicants error:", err)
					}
				}(i)
			}
		}
	}
	return nil
}

func (this *Node) PutData(arg Data, ret *Res) error {
	c := this.put_db(arg)
	*ret = <-c
	if ret.Err == nil {
		var num_replikas int = 0 // count the number of replicas
		for i := 0; i < r; i++ {
			if this.succList[i].Empty() == false {
				var result Res //RPC return value
				client, err := rpc.DialHTTP("tcp", this.succList[i].Ip+":"+this.succList[i].Port)
				if err != nil {
					log.Println("PutData, Replikering "+"dialing:", err)
					continue
				}
				err = client.Call("Node.PutReplicateData", arg, &result) // put this data on another node.
				if err != nil {
					log.Println("PutData, Replikering "+"PutReplicateData error:", err)
				} else {
					num_replikas++ // a replika was made
				}
				err = client.Close()
				if err != nil {
					log.Println("PutData, Replikering "+"closing error:", err)
				}

			}

		}
	}

	return nil
	// if ret.Err != nil {
	// 	return ret.Err
	// } else {
	// 	return nil
	// }
}

func (this *Node) DelData(arg Data, ret *Data) error {
	return nil
}

func (this *Node) ModData(arg Data, ret *Res) error {
	c := this.mod_db(arg)
	*ret = <-c
	if ret.Err == nil {
		// Proceed with modifying the entry on each of the replicants
		var num_replikas int = 0 // count the number of replicas
		for i := 1; i < r; i++ {
			if this.succList[i].Empty() == false {
				var result Res                                                                    //RPC return value
				client, err := rpc.DialHTTP("tcp", this.succList[i].Ip+":"+this.succList[i].Port) // connect to a replicant
				if err != nil {
					log.Println("ModData, Replikering "+"dialing:", err)
					continue
				}
				err = client.Call("Node.PutReplicateData", arg, &result) // Mode the entry on the replicant.
				if err != nil {
					log.Println("ModData, Replikering "+"ModReplicateData error:", err)
				} else {
					num_replikas++ // a replika was made
				}
				err = client.Close()
				if err != nil {
					log.Println("ModData, Replikering "+"closing error:", err)
				}
			}
		}
	}
	return nil
}

// Replication: Insert funktion for storing the data
func (this *Node) PutReplicateData(arg Data, ret *Res) error {
	fmt.Println("Storing replicated data:", arg)
	c := this.put_db(arg)
	*ret = <-c
	return nil
}

// Replication: Modification funktion for changing the data on a replicant node
func (this *Node) ModReplicateData(arg Data, ret *Res) error {
	fmt.Println("Modifying replicated data with key:", arg.Key)
	c := this.mod_db(arg)
	*ret = <-c
	return nil
}

// Replication: Delete the bucket that reprisents a dead node.
// This function is called after the values have been absorbed into the bucket of the node that is now responsible of the values
func (this *Node) DeleteDeadBucket(bucketName []byte, ret *Res) error {
	fmt.Println("Deleting a dead bucket")
	c := this.delete_bucket(bucketName)
	*ret = <-c
	return nil
}
