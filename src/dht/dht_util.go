package dht

import (
	"crypto/sha1"
	"fmt"
	"github.com/nu7hatch/gouuid"
	"log"
	"math/big"
	"net/rpc"
)

//Calculate distance between node id a and b, taken into account that there is a
//wrap-round when we reach 2^m such that the ring continues at 0
func distance(a, b []byte, bits int) *big.Int {
	var ring big.Int
	ring.Exp(big.NewInt(2), big.NewInt(int64(bits)), nil)

	var a_int, b_int big.Int
	(&a_int).SetBytes(a)
	(&b_int).SetBytes(b)

	var dist big.Int
	(&dist).Sub(&b_int, &a_int)

	(&dist).Mod(&dist, &ring)
	return &dist
}

//a is a node ID/hash value.
//Calculates a hash on the ring that is 2^i steps behind a.
func subDistance(a big.Int, i, m int) big.Int {
	var ring big.Int
	ring.Exp(big.NewInt(2), big.NewInt(int64(m)), nil) //Size of ring: 2^m

	var sub_int big.Int

	(&sub_int).Exp(big.NewInt(2), big.NewInt(int64(i)), nil) // sub_int = 2^i

	var dist big.Int
	(&dist).Sub(&a, &sub_int) // dist = a - 2^i
	(&dist).Mod(&dist, &ring) // dist = dist mod 2^m => (a - 2^i) mod 2^m
	//This handles the problem of walking backwards in the ring past hash 0
	return dist
}

// checks if key is between (id1<1d2]
func betweenTo(id1, id2, key big.Int) bool {
	// 0 if a==b, -1 if a < b, and +1 if a > b
	if key.Cmp(&id2) == 0 { // key == id2
		return true
	}

	if id2.Cmp(&id1) == 1 { // id2 > id1
		if key.Cmp(&id2) == -1 && key.Cmp(&id1) == 1 { // key < id2 && key > id1
			return true
		} else {
			return false
		}
	} else if id1.Cmp(&id2) == 1 { // id1 > id2
		if key.Cmp(&id1) == 1 || key.Cmp(&id2) == -1 { // key > id1 || key < id2
			return true
		} else {
			return false
		}
	}
	return false
}

// checks if key is between [id1<1d2)
func betweenFrom(id1, id2, key big.Int) bool {
	// 0 if a==b, -1 if a < b, and +1 if a > b
	if key.Cmp(&id1) == 0 { // key == id1
		return true
	}

	if id2.Cmp(&id1) == 1 { // id2 > id1
		if key.Cmp(&id2) == -1 && key.Cmp(&id1) == 1 { // key < id2 && key > id1
			return true
		} else {
			return false
		}
	} else if id1.Cmp(&id2) == 1 { // id1 > id2 Check if the interval rolls over to the beginning of the circle.
		if key.Cmp(&id1) == 1 || key.Cmp(&id2) == -1 { // key > id1 || key < id2
			return true
		} else {
			return false
		}
	}

	return false
}

// checks if key is IN BETWEEN (id1<1d2)
func inbetween(id1, id2, key big.Int) bool {
	// 0 if a==b, -1 if a < b, and +1 if a > b

	if key.Cmp(&id2) == 0 { // key == id2
		return false
	}
	/////////////////////////////////////////////////////////////////////////////////////////////// id1 > id2
	if id2.Cmp(&id1) == 1 { // id2 > id1
		if key.Cmp(&id2) == -1 && key.Cmp(&id1) == 1 { // key < id2 && key > id1
			return true
		} else {
			return false
		}
	} else if id1.Cmp(&id2) == 1 { // id1 > id2
		if key.Cmp(&id1) == 1 || key.Cmp(&id2) == -1 { // key > id1 || key < id2
			return true
		} else {
			return false
		}
	}

	return false
}

// (n + 2^(k-1)) mod (2^m)
// n = ID of the node, index = finger index, m = number of bites (160 for SHA-1)
//calculate the node-position of the key index "index"
func calcFingerStart(n big.Int, index int, m int) big.Int { //Tidigare return type: (string, big.Int)

	// get the right addend, i.e. 2^(index-1)
	two := big.NewInt(2)
	addend := big.Int{}
	addend.Exp(two, big.NewInt(int64(index-1)), nil)

	// calculate sum
	sum := big.Int{}
	sum.Add(&n, &addend)

	// calculate 2^m
	ceil := big.Int{}
	ceil.Exp(two, big.NewInt(int64(m)), nil)

	// apply the mod
	result := big.Int{}
	result.Mod(&sum, &ceil)

	return result
}

// m-bit identifier
func generateNodeId() string {
	u, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}

	// calculate sha-1 hash
	hasher := sha1.New()

	hasher.Write([]byte(u.String()))

	return fmt.Sprintf("%x", hasher.Sum(nil))
}

func (this *Data) hashit() (hash big.Int) {
	hasher := sha1.New()

	hasher.Write([]byte(this.Key))
	hash.SetBytes(hasher.Sum(nil))
	return
}

func (this *Bro) hashit() (hash big.Int) {
	hasher := sha1.New()

	hasher.Write(this.Id.Bytes())
	hash.SetBytes(hasher.Sum(nil))
	return
}

func (this *Node) printFingerTable() {
	fmt.Println("Printar finger table för nod: ", this.id.Bytes())
	fmt.Println("Node", this.id.String())
	for i := 0; i < len(this.finger); i++ {
		fmt.Printf("Finger %d --> Start: %d  Successor: %d\n", i+1, this.finger[i].start.String(), this.finger[i].succ.Id.String())
	}
}

func (this *Node) printRing() {
	node := this.succ
	fmt.Println(this.id.String())
	for node.Id.Cmp(&this.id) != 0 {
		fmt.Println(node.Id.String())
		var temp_succ Bro
		client2, err2 := rpc.DialHTTP("tcp", node.Ip+":"+node.Port)
		if err2 != nil {
			log.Fatal("dialing:", err2)
		}
		err2 = client2.Call("GetSucc", Args{}, &temp_succ)
		if err2 != nil {
			log.Fatal("GetSucc error:", err2)
		}
		err2 = client2.Close()
		if err2 != nil {
			log.Fatal("Closing conn error:", err2)
		}
		*node = temp_succ
	}
}
