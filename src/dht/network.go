package dht

import (
	_ "encoding/json"
	_ "fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	_ "net/rpc/jsonrpc"
	"os"
)

/*
we choose to use TCP connection to get reliable connection and dont have to handle request-timeouts in our implementation etc.
*/

//init for every node
func (this *Node) listen() {

	//creates a new RPC server and register our node "this" to that server
	server := rpc.NewServer()
	//publishes the set of methods of 'this' that satisfies the four RPC conditions
	server.Register(this)
	//registers a handler for RPC messages
	server.HandleHTTP(rpc.DefaultRPCPath, rpc.DefaultDebugPath)

	//creates servers
	address := "0.0.0.0" + ":" + this.port
	var e error
	this.listener, e = net.Listen("tcp", address)
	if e != nil {
		log.Fatal("error in network.listen() function. Error: ", e)
	}

	//accepts incoming HTTP-connections on the listener, creating a new service goroutine for each.
	//The service goroutines read requests and then call handler to reply to them.
	go http.Serve(this.listener, nil)

}

func (this *Node) StopListen(arg Args, out *string) error {
	defer this.listener.Close()
	defer os.Exit(2)
	return nil
}

func (this *Node) discoverBro(ip, port string) Bro {
	// Discover another node in order to attach this node to the ring
	var reply Bro //RPC return value
	address := ip + ":" + port
	client, err := rpc.DialHTTP("tcp", address)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	err = client.Call("Node.DiscoverResponder", Args{}, &reply)
	if err != nil {
		log.Fatal("DiscoverResponder error:", err)
	}
	err = client.Close()
	if err != nil {
		log.Fatal("Closing conn error:", err)
	}
	return reply
}
