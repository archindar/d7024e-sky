package dht

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/rpc"
	"os"
)

func response(key, value string) {

}

var server_err string = "server: "

//Present the initial web interface from the HTML file test.html
func submitHandler(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadFile("./dht/test.html")
	fmt.Fprintf(w, "%s <br><h3>This server has the RPC port: %s</h3>", body, glob_port)
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	Key := r.FormValue("nyckel")
	Value := r.FormValue("vrde")
	// body, _ := ioutil.ReadFile("./dht/result.html")

	var result Res //RPC return value
	inp := Data{Key: Key, Value: Value}
	client, err := rpc.DialHTTP("tcp", glob_ip+":"+glob_port) // Connect to the local chord program
	if err != nil {
		log.Panic(server_err+"dialing:", err)
	}
	err = client.Call("Node.Web_send", inp, &result) // put the data on another node.
	if err != nil {
		log.Panic(server_err+"PutData error:", err)
	}
	err = client.Close()
	if err != nil {
		log.Panic(server_err+"Closing conn error:", err)
	}
	if result.Succ == 1 { // insertion succeeded
		fmt.Fprintf(w, "<p><h1>You uploaded value: %s with key: %s</h1></p>"+
			"<br>The data was stored on the node with the RPC port: %s", Value, Key, result.Ds.Key)
	} else {
		fmt.Fprintf(w, "<p><h1>Uppload failed</h1></p><p>Error:%s</p><div>", result.Err)
	}

	magicalButtonFixer(w)

}

func lookupHandler(w http.ResponseWriter, r *http.Request) {
	Key := r.FormValue("data")
	fmt.Println("Lookup of data: ", Key)

	var result Res //RPC return value
	inp := Data{Key: Key}
	client, err := rpc.DialHTTP("tcp", glob_ip+":"+glob_port) // Connect to the local chord program
	if err != nil {
		log.Panic(server_err+"dialing:", err)
	}
	err = client.Call("Node.Web_lookup", inp, &result) // put the data on another node
	if err != nil {
		log.Panic(server_err+"GetData error:", err)
	}
	err = client.Close()
	if err != nil {
		log.Panic(server_err+"Closing conn error:", err)
	}

	if result.Succ == 1 { // Retrieval success
		fmt.Fprintf(w,
			"<p><h2>The data you searched for: %s</h2></p>"+
				"<p><h2> The key used in the search: %s</h2></p>"+
				"<br>The data was retrieved from the node with the RPC port: %s", result.Ds.Value, Key, result.Ds.Key)
	} else if result.Succ == 2 {
		fmt.Fprintf(w,
			"<p><h2>The data you searched for: %s</h2></p>"+
				"<p><h2> The key used in the search: %s</h2></p><br>This is the replicated data."+
				" By now the data have been absorbed", result.Ds.Value, Key)
	} else {
		fmt.Fprintf(w, "<p><h1>Lookup failed</h1></p><p>Error:%s</p>", result.Err)
	}

	magicalButtonFixer(w)

}

func updateHandler(w http.ResponseWriter, r *http.Request) {
	update_key := r.FormValue("update_key")
	update_value := r.FormValue("update_value")

	var result Res //RPC return value
	inp := Data{Key: update_key, Value: update_value}
	client, err := rpc.DialHTTP("tcp", glob_ip+":"+glob_port) // Connect to the local chord program
	if err != nil {
		log.Panic(server_err+"dialing:", err)
	}
	err = client.Call("Node.Web_update", inp, &result) // put the data on another node.
	if err != nil {
		log.Panic(server_err+"PutData error:", err)
	}
	err = client.Close()
	if err != nil {
		log.Panic(server_err+"Closing conn error:", err)
	}
	if result.Succ == 1 {
		fmt.Fprintf(w,
			"<p><h2>You updated key: %s with the new value: %s</h2></p>", update_key, update_value)
	} else {
		fmt.Fprintf(w, "<p><h1>Update failed</h1></p><p>Error:%s</p>", result.Err)
	}

	magicalButtonFixer(w)
}

func deleteHandler(w http.ResponseWriter, r *http.Request) {
	delete_data := r.FormValue("deletedata")
	fmt.Println("Deletion of data: ", delete_data)

	fmt.Fprintf(w,
		"<p><h2>You deleted value: %s which had the key: %s</h2></p>", delete_data, "key")

	magicalButtonFixer(w)
}

func shutdownHandler(w http.ResponseWriter, r *http.Request) {
	//node_shutdown := r.FormValue("shutdown_node")
	var result string
	client, err := rpc.DialHTTP("tcp", glob_ip+":"+glob_port) // Connect to the local chord program
	if err != nil {
		log.Panic(server_err+"Shutdown dialing:", err)
	}
	err = client.Call("Node.StopListen", Args{}, &result) // put the data on another node.
	if err != nil {
		log.Panic(server_err+"Shutdown call error:", err)
	}
	err = client.Close()
	if err != nil {
		log.Panic(server_err+"Shutdown Closing conn error:", err)
	}
	fmt.Fprintf(w,
		"<p><h2>You just shut down the local node</h2></p>")

	magicalButtonFixer(w)

}

func magicalButtonFixer(w http.ResponseWriter) {
	button, _ := ioutil.ReadFile("./dht/result.html")
	fmt.Fprintf(w, "%s", button)
}

func runServer() {
	http.HandleFunc("/submit/", submitHandler)
	http.HandleFunc("/upload/", uploadHandler)
	http.HandleFunc("/lookup/", lookupHandler)
	http.HandleFunc("/update/", updateHandler)
	http.HandleFunc("/delete/", deleteHandler) //Add a handler to DefaultServeMux :)
	http.HandleFunc("/shutdown/", shutdownHandler)

	if len(os.Args) > 1 { // listen to the port set by commandline arguments
		if os.Args[1] != "0" {
			http.ListenAndServe(":"+os.Args[1], nil) //Starts a HTTP server with address and handler = oftast nil-> DefaultServeMux
		} else if os.Getenv("webPORT") != "0" {
			http.ListenAndServe(":"+os.Getenv("webPORT"), nil)
		} else {
			http.ListenAndServe(":8082", nil)
		}

	} else if os.Getenv("webPORT") != "0" { // If the webPORT environment variable set
		http.ListenAndServe(":"+os.Getenv("webPORT"), nil) //Starts a HTTP server with address and handler = oftast nil-> DefaultServeMux
	} else { // default setting
		http.ListenAndServe("localhost:8082", nil) //Starts a HTTP server with address and handler = oftast nil-> DefaultServeMux
	}
}
