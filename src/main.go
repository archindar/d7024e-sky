package main

import (
	"dht"
	"fmt"
	_ "net"
	"time"
)

func main() {
	fmt.Println("Start of program")
	dht.Main()
	c := time.Tick(3 * time.Minute)
	for now := range c {
		fmt.Println("Still allive:", now)
	}
}
