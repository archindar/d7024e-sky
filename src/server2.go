package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
)

func main() {
	http.HandleFunc("/input/", inputHandler)
	http.HandleFunc("/send/", sendHandler)
	http.ListenAndServe(":8000", nil)

}

func inputHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadFile("nodeform.html")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, "%s", body)
}

func sendHandler(w http.ResponseWriter, r *http.Request) {
	web_port := r.FormValue("webport")
	new_ip := r.FormValue("newip")
	new_port := r.FormValue("newport")
	ip := r.FormValue("ip")
	port := r.FormValue("port")

	if r.FormValue("webport") != "" && r.FormValue("newip") != "" && r.FormValue("newport") != "" && r.FormValue("ip") != "" && r.FormValue("port") != "" {
		runCmd(web_port, new_ip, new_port, ip, port)
	} else if r.FormValue("webport") != "" && r.FormValue("newip") != "" && r.FormValue("newport") != "" {
		runCmd2(web_port, new_ip, new_port)
	}
}

//Invoke the docker command at the cmd, send user input as args
func runCmd(webport, ip1, port1, ip2, port2 string) {
	fmt.Println("runCmd körs")

	dockerCmd := "sudo docker run -d -e WEB_PORT=" + webport + " -e NEW_IP=" + ip1 + " -e NEW_PORT=" + port1 + " -e IP=" + ip2 + " -e PORT=" + port2 + "-p " + webport + ":" + webport + " -p " + port1 + ":" + port1 + " finalimage"

	//Runs the docker command and returns the output
	os.Chdir("/home/ubuntu/")
	output, err := exec.Command("/bin/sh", "-c", dockerCmd).Output()
	fmt.Println("Output of docker command: ", output)
	//err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}

func runCmd2(webport, ip1, port1 string) {
	fmt.Println("runCmd körs")

	dockerCmd := "sudo docker run -d -e WEB_PORT=" + webport + " -e NEW_IP=" + ip1 + " -e NEW_PORT=" + port1 + "-p 8082:" + " -p " + port1 + ":" + port1

	//Runs the docker command and returns the output
	output, err := exec.Command("/bin/sh", "-c", dockerCmd).Output()
	fmt.Println("Output of docker command: ", output)
	//err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}
